import React from 'react';
import { ScrollView, StyleSheet, Image } from 'react-native';
import { ExpoLinksView } from '@expo/samples';

export default class LinksScreen extends React.Component {
  static navigationOptions = {
    title: 'Links',
  };

  render() {
    return (
      <ScrollView style={styles.container}>
        {/* Go ahead and delete ExpoLinksView and replace it with your
           * content, we just wanted to provide you with some helpful links */}
        <ExpoLinksView />
        <Image
          style={{width: '100%', height: 200,resizeMode : 'stretch' }}
          source={{uri: 'https://i-vnexpress.vnecdn.net/2019/01/11/A5OKVVNYEVHJPEBGW44JZFTV5I3937-7090-6446-1547182287_180x108.jpg'}} 
        /> 
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: '#fff',
  },
});
