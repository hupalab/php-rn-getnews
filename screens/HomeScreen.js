import React, { Component } from 'react'
import { Text, View, FlatList, Image, Avatar } from 'react-native'
import { getNews } from './api/getnews'
import { List, ListItem } from 'react-native-elements'

export default class HomeScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      refreshing: false,
      p:''

    }
    this.getMoviesFromApiAsync = this.getMoviesFromApiAsync.bind(this);
  }
componentDidMount = () => {
  this.getMoviesFromApiAsync()
}

  // fetch("http://127.0.0.1/LayTin/")
  // .then(response => {
  //   console.log(response);
  //   return response.json();
  // })
  // .then(responseJson => {
  //   this.setState({ data: responseJson });
  // })
   getMoviesFromApiAsync= async (p) => {
    try {
       const responseJson = await getNews(p)
       .then(result => {
          this.setState({ data:result });

        });
        // console.log(responseJson);
     }
     catch (error) {
       console.error(error);
     }
  }
  handleRefresh() {
    this.setState(
      {
        refreshing: true,
        p:this.state.p+1,
    },
      () => {this.getMoviesFromApiAsync(this.state.p); this.setState({refreshing: false}); }
    );
  }
  renderRow ({ item }) {
    return (
      <ListItem
        roundAvatar
        title={item.TITLE}
        subtitle={
          <Text style={{color: "green"}}>{item.DES}</Text>
        }
        leftAvatar={{ 
          source: { uri: item.IMG },
          rounded: true, 
          size:"large",
          
          
        }}
        onPress={() => console.log("Works!")}
        activeOpacity={0.8}
      />

    )
  }

  render() {
    return (
      <View>
         <FlatList
           data={this.state.data}
           keyExtractor={item => item.KEY}
           renderItem={this.renderRow}
          refreshing={this.state.refreshing}
          onRefresh={this.handleRefresh.bind(this)}

        />
      </View>
    )
  }
}


